/**
 * Created by wisdomwolf on 6/18/2016.
 */
$(document).ready(function() {
    $("#unit_number_search").keypress(function (e) {
        if ((e.which && e.which == 13) || (e.keyCode && e.keyCode == 13)) {
            goSearch($('#unit_number_search').val());
            return false;
        } else {
            return true;
        }
    });
    disableFields(true);
    $('.btnsubmit').each(function() {
        $(this).on("click", editButtonHandler);
    });
    $('.shareStatus').each(function() {
        $(this).on('change', updateSharepoint);
    });
    $('#btnAddYes').on('click', function() {
        var url = Flask.url_for("unit_info");
        window.location.href = url;
    });
    $('#add_unit_question').hide();
    $('.remedy_task').on('click', openRemedyTask);
    setTimeout(goSearch, 500);
    hideFields();
    $("button.shownextrow").on('click', showNextRow);
    $("button.removethisrow").on('click', removeCurrentRow);
});